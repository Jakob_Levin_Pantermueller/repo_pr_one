﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public class EntityManager : MonoBehaviour, ILog
    {

        public void Log(object needsToBeLogged)
        {
            Debug.Log(needsToBeLogged);
        }

        public string[] entities = new string[6]
        {
            "Mann",
            "Frau",
            "Kind",
            "Großmutter",
            "Tante",
            "Cousine "
        };

        public void Start()
        {
            Debug.Log("Namen werden registriert");

            for (int i = 0; i < entities.Length; i++)
            {
                Debug.Log("Die Waffe auf Indexplatz: " + i + " / gehört zu dieser Klasse: " + entities[i]);
            }
        }
    }
}