﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public class Player : Entity, IAttack
    {
        private int currentHealth;
        private int attackAmount;
        private bool isAlive;
        Enemy enemyEntity;
        Entity entity;

        public void AttackEnemy(Enemy enemy, int amountAttackDamage)
        {
            if(enemyEntity.IsAlive)
            {
                enemyEntity.Damage(10);
            }
        }

        public void Start()
        {
            Log(EntityName);
            Log(currentHealth);
            Log(attackAmount);
            AttackEnemy(enemyEntity, 10);
        }
    }
}